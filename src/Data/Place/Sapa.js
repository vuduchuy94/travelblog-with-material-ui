const SapaJSON = {
    pageInfo: {
      title: "トーへ",
      keywords: ["伝統", "文化", "食べ物", "トーへ"],
      description:
        "日本の飴細工をご存知でしょうか？ベトナムにも飴細工みたいなものがありますよ。但し、 飴細工と違う点のはこれが麦粉で作られます(笑笑)。それでは、この記事はベトナムのトーへを紹介させて頂きます。"
    },
    header: {
      title: "ベトナム旅行ならSapaへ",
      subtitle: "Vietnamese Culture",
      byline: { date: { datetime: "2020-07-03", text: "July 03, 2020" } },
      image: { src: "Places/Sapa/sapa.jpg", alt: "Sapa" }
    },
    sections: [
      // TODO: p and image are in diffent sections?
      {
        type: "Description",
        paragraphs: [
            "サパはベトナムの首都ハノイより約250km、標高1,600mの山間部にある高原リゾートです。",
            "ベトナムには54の民族があり、その中でもサパ周辺には数多くの少数民族が生活しております。サパ観光中には、数多くの少数民族に出会うことが出来、少数民族特有の華やかな衣装に目を奪われる事でしょう。",
            "サパの目玉の一つとして、各国の写真家も多く訪れて撮影をする棚田を見逃すことは出来ません。4月から田植えが始まり、稲が生長し緑が生える夏場、黄金色に稲穂が輝く9月とどの時期に訪れても素晴らしい景観を見れることができます。",
            "そんなサパに訪れるともう一度訪れたいと思ってしまうほどの魅力を秘めております。"
        ],
        image: { src: "Places/Sapa/sapa1.jpg", alt: "Sapa" }       
      },
      {
        type: "Images",
        heading: "段々畑",
        images: [
          { src: "Places/Sapa/ruong-bac-thang.jpg", alt: "" },
          { src: "Places/Sapa/ruong-bac-thang1.jpg", alt: "" },
          { src: "Places/Sapa/ruong-bac-thang2.jpg", alt: "" },
        ],
        text: [
            "青々とした緑が生い茂る夏の棚田（5～6月）も、稲穂により黄金色に染まる秋の棚田（8～9月）も、雪に覆われて白く輝く冬の棚田（11～12月）も、季節ごとに出会えるサパの心癒される絶景です。",
            "山々に囲まれた、絵葉書のように美しいサパの棚田に会いに来てください。"
        ],
      },
      {
        type: "Images",
        heading: "愛の滝",
        images: [
          { src: "Places/Sapa/thac-tinh-yeu.jpg", alt: "" },
          { src: "Places/Sapa/thac-tinh-yeu1.jpg", alt: "" },
          { src: "Places/Sapa/thac-tinh-yeu2.jpg", alt: "" },
        ],
        text: [
            "滝から流れる清い水には金の渓流という名前がつけられています。とても静かできれいなところです。"
        ],
      },
    ]
  };

  export default SapaJSON;
  