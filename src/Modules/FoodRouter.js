import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import AllFood from "./AllFood";
import Tohe from "../Pages/Food/Tohe";
import Bunbohue from "../Pages/Food/Bunbohue"

const FoodRouter = () => (
  <BrowserRouter>
    <Switch>
    <Route exact path="/food" component={AllFood} />
    <Route path="/food/tohe" component={Tohe} />
    <Route path="/food/bunbohue" component={Bunbohue} />
  </Switch></BrowserRouter>

);

export default FoodRouter;
