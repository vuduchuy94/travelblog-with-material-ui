import React from "react";
import { BrowserRouter,Switch, Route } from "react-router-dom";
import Home from "./Home";
import FoodRouter from "./FoodRouter";
import PlaceRouter from "./PlaceRouter";

const AllRouters = () => (
  <main>
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/place" component={PlaceRouter} />
        <Route path="/food" component={FoodRouter} />
      </Switch>
    </BrowserRouter>
  </main>
);

export default AllRouters;
