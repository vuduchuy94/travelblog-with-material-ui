import React from "react";
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    root: {
      backgroundColor: "#fff",
      color: "#000",
      paddingTop: "5rem",
      paddingBottom: "5rem",
      textAlign:"center",
      // position:"absolute",
      // bottom:"5px",
          "& ul":{
        textAlign:"center",
        alignItems: "center",
        flexDirection: "column",
        marginBottom: "1.5rem",
        display:"inline-block",
        flexWrap:"wrap",
        '& li':{
           marginRight:"2rem",
           display:"inline-block",
           
        }}
    }
  }));

export default function Footer(props) {
  const classes = useStyles();

  return (
    <footer className={classes.root}>
            <nav>
        <ul>
          <li>
            <a>運営会社</a>
          </li>
          <li>
            <a>プライバシーポリシー</a>
          </li>
          <li>
            <a>お問い合わせ</a>
          </li>
        </ul>
      </nav>
      <small>
        Copyright © 2020 旅時間 All Rights Reserved.
      </small>

    </footer>
  );
}