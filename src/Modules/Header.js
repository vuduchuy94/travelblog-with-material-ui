import React from "react";
import { Link } from "react-router-dom";
import PropTypes from 'prop-types';
import "./Header.css";
import { withStyles } from '@material-ui/core/styles';

const styles = {
    root: {
      flexGrow: 1
    },
    content:{
      margin: 'auto',
      maxWidth: 700
    },
    menuItem:{
      display:"inline-block"  
    },
    menuItems:{
      display:"inline-block",
      paddingLeft: "0px"
    },
    link:{
      textDecoration:"none"
    }
};
  
function Header(props){
  const { classes } = props;

  return (
    <header className={classes.root}>
      <nav id="pageHeader" className={classes.content}>
        <ul className={classes.menuItems}>
          <li className={classes.menuItem}>
            <Link className={classes.link} to="/">Home</Link>
          </li>
          <li>
            <Link to="/food">Food</Link>
          </li>
          <li>
            <Link to="/place">Place</Link>
          </li>
        </ul>
      </nav>
  </header>
  );
}
  
export default withStyles(styles)(Header);