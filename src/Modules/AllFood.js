import React from "react";
import { Link } from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Header from "./Header";
import Footer from "./Footer";

import BunbohueImage from "../Img/Foods/Bunbohue/bunbohue1.jpg";
import ToheImage from "../Img/Foods/Tohe/tohe.jpg";

const useStyles = makeStyles({
  root: {
    flexFlow: 1
  },
  card: {
    margin: "10px 20px",
    padding: "10px",
    width: "300px"
  },
  media: {
    height: "150px",
  }
});
const allFood=[
  {id:"tohe", title:"Tò he", image: ToheImage},
  {id:"bunbohue", title:"ブンボーフェ", image: BunbohueImage}
];

const allFoods=[
  {id:"tohe", title:"Tò he", image: ToheImage},
  {id:"bunbohue", title:"ブンボーフェ", image: BunbohueImage}
];

function AllFood(props){
  const classes = useStyles();

  
  return (
    <React.Fragment>
    <Header />
    <Grid container className={classes.root} spacing={1}>
      <Grid item xs={12}>
        <Grid container justify="center">
          {allFoods.map(food => (
            <Grid>
              <Card className={classes.card}>
              <CardActionArea>
              <CardMedia
                component="img"
                className={classes.media}
                image = {food.image}
                title={food.title}
              />
              <CardContent>
                <Typography gutterBottom variant="h5" component="h2">
                {food.title}
                </Typography>
              </CardContent>
              </CardActionArea>
              <CardActions>
                <Button size="small" color="primary">
                  <Link to={`/food/${food.id}`}>View More</Link>
                </Button>
              </CardActions>
             </Card>
            </Grid>
          ))}
        </Grid>
      </Grid>
     </Grid> 
     <Footer/>
    </React.Fragment>
  )
};

export default AllFood;
