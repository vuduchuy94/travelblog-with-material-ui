import Page from "../../Page";
import SapaJSON from "../../Data/Place/Sapa";

const Sapa = props => {
  return Page({ page: SapaJSON });
};

export default Sapa;
