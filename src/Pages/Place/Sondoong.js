import Page from "../../Page";
import SondoongJSON from "../../Data/Place/Sondoong";

const Sondoong = props => {
  return Page({ page: SondoongJSON});
};

export default Sondoong;
