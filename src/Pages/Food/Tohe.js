import Page from "../../Page";
import ToheJSON from "../../Data/Food/Tohe";

const Tohe = props => {
  return Page({ page: ToheJSON });
};

export default Tohe;
