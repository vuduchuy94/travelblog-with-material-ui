import './App.css';
import AllRouters from './Modules/AllRouters';
import React from "react";
 
 
function App() {
   return (
   <div className = "App" >
       <AllRouters/>
   </div>
   );
}
 
export default App;
